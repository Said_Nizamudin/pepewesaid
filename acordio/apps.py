from django.apps import AppConfig


class AcordioConfig(AppConfig):
    name = 'acordio'
