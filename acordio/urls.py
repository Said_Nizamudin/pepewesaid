from django.urls import path
from . import views

app_name = "acordio"

urlpatterns = [
    path('', views.index, name='index'),
    path('boot', views.boot, name='boot'),
]