from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.

def index(request):
    return render(request, 'indexAcordio.html')

def boot(request):
    return render(request, 'indexAcordioBoot.html')