from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from .views import *
from .forms import *
from django.apps import apps
from .apps import LoginConfig
from django.http import HttpRequest



# Create your tests here.
class LoginTest(TestCase):

    def test_app_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 301)

    def test_function_index(self):
        found = resolve('/login/')
        self.assertEqual(found.func, index)

    def test_function_login(self):
        found = resolve('/login/login/')
        self.assertEqual(found.func, login)

    def test_function_logout(self):
        found = resolve('/login/logout/')
        self.assertEqual(found.func, logout)
        
    def test_function_signup(self):
        found = resolve('/login/signup/')
        self.assertEqual(found.func, signup)

    def test_landing_page_is_using_correct_html(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'awalan.html')

    def test_login_page_url_is_exist(self):
        response = Client().get('/login/login/')
        self.assertEqual(response.status_code, 200)

    def test_login_page_is_using_correct_html(self):
        response = Client().get('/login/login/')
        self.assertTemplateUsed(response, 'login.html')
    
    def test_page_template_logout(self):
        response = Client().get('/login/logout/')
        self.assertEqual(response.status_code,302)
    
    def test_story9_url_is_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)
        self.assertTemplateUsed(response, 'awalan.html')

    def test_signup_page_url_is_exist(self):
        response = Client().get('/login/signup/')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(LoginConfig.name, 'login')
        self.assertEqual(apps.get_app_config('login').name, 'login')

    def test_form_post(self):
        signUp_form = SignUpForm(data={'email':'abcd@gmail.com'})
        self.assertFalse(signUp_form.is_valid())
        self.assertEqual(signUp_form.cleaned_data['email'],"abcd@gmail.com")

   
