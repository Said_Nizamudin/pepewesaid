from django.urls import path
from . import views

app_name = "sosmed"

urlpatterns = [
    path('', views.index, name='index'),
]