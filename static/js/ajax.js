function ajaxx() {  
  $.ajax({
    url: 'https://www.googleapis.com/books/v1/volumes',
    type: 'get',
    dataType: 'json',
    data: {
        'q': $('#form').val(),
    },
    success: function(result) {
        const book = result.items
        $("#buku").html('')
        $.each(book, function(i, data){
            const info = data.volumeInfo
            var penulis = ''
            $.each(info.authors, function(i, data){
                if (i === 0) {
                    penulis += data
                } else {
                    penulis += ', ' + data
                }
            })
            $("#buku").append(`
                <div class="card mb-5">
                    <div class="card-body">
                        <h5 class="card-title">${info.title}</h5>
                        <span class="card-publisher">${info.publisher}</span><br><br>
                        <span class="card-author"><strong>${penulis}</strong></span>
                    </div>
                </div>
            `)
        })
    }
  })
}
 

$("#button").on("click", function() {
  ajaxx()
})